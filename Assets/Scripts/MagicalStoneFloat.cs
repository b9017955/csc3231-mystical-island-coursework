using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Rotates the stones around rotationCentre & makes them bob up and down whilst doing so
public class MagicalStoneFloat : MonoBehaviour
{
    public Transform rotationCentre;
    public float rotationSpeed;
    public float oscillationOffset;

    // Update is called once per frame
    void Update()
    {
        transform.RotateAround(rotationCentre.position, Vector3.up, Time.deltaTime * rotationSpeed);
        Vector3 translation = new Vector3(
            0, 
            0.5f * Mathf.Sin(Time.timeSinceLevelLoad + oscillationOffset)* Time.deltaTime,
            0
        );

        transform.Translate(translation, Space.World);
    }
}
