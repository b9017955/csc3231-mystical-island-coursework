using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//This class causes the camera to render the depthtexture which is used by my water shader to calculate the fragment colour
[ExecuteInEditMode]
public class DepthTextureManager : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        GetComponent<Camera>().depthTextureMode = DepthTextureMode.Depth;
    }
}
