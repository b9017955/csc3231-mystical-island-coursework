using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Simple script to rotate the gameobject about the y axis, in a given direction & angular speed
public class Rotate : MonoBehaviour
{
    public bool clockwise;
    public float rotationRate;

    // Update is called once per frame
    void Update()
    {
        float rotationDirection = clockwise ? 1f : -1f;
        transform.Rotate(0, rotationDirection * rotationRate * Time.deltaTime, 0, Space.World);
    }
}
