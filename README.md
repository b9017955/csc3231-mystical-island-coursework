You must open SampleScene in "Assets/Scenes"

External material is located in "Assets/Third Party":

- Classic Sky Box by mgsvevo: https://assetstore.unity.com/packages/2d/textures-materials/sky/classic-skybox-24923
- GrassFlowers by ALP8310: https://assetstore.unity.com/packages/2d/textures-materials/nature/grass-flowers-pack-free-138810
- HighQualityBricks&Walls by Alessio Regalbuto: https://assetstore.unity.com/packages/2d/textures-materials/brick/high-quality-bricks-walls-49581
- laxer tree pkg by Laxer: https://assetstore.unity.com/packages/3d/vegetation/trees/mobile-tree-package-18866
- LowlyPoly by LowlyPoly: https://assetstore.unity.com/packages/2d/textures-materials/free-stylized-textures-204742
- Mountain Terrain by CG Creative Sets: https://assetstore.unity.com/packages/3d/environments/landscapes/mountain-terrain-rock-tree-97905
- Sand textures pack by Nobiax / Yughues: https://assetstore.unity.com/packages/2d/textures-materials/floors/yughues-free-sand-materials-12964
- Stone by Pixel Games: https://assetstore.unity.com/packages/3d/props/exterior/stone-651
- TerrainTexturesPackFree by ALP8310: https://assetstore.unity.com/packages/2d/textures-materials/nature/terrain-textures-pack-free-139542
- Tree9 by Pixel Games: https://assetstore.unity.com/packages/3d/vegetation/trees/realistic-tree-9-rainbow-tree-54622
- Unity Standard Assets by Unity Technologies: https://assetstore.unity.com/packages/essentials/asset-packs/standard-assets-for-unity-2018-4-32351
