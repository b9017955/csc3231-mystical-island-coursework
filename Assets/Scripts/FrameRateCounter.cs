using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Profiling;

//Outputs framerate and memory statistics
public class FrameRateCounter : MonoBehaviour
{
    [Header("Unity Attributes")]
    public UnityEngine.UI.Text frameRateCounter;
    public UnityEngine.UI.Text reserveMemory;
    public UnityEngine.UI.Text usedMemory;

    [Header("UI Attributes")]
    public float uiRefreshesPerSecond=2f;
    public int numberOfFramesToAverage = 5;

    float uiRefreshTimer;
    int[] frameRates;//collects fps values to calculate an average
    int frameRateIterator;//counts the number of frames currently used to calculate the current average

    ProfilerRecorder totalReservedMemory;
    ProfilerRecorder currentMemoryUsage;

    private void Start()
    {
        frameRates = new int[numberOfFramesToAverage];

        //Start the performance profilers
        totalReservedMemory = ProfilerRecorder.StartNew(ProfilerCategory.Memory, "Total Reserved Memory");
        currentMemoryUsage = ProfilerRecorder.StartNew(ProfilerCategory.Memory, "Total Used Memory");
        totalReservedMemory.Start();
        currentMemoryUsage.Start();
    }

    // Start is called before the first frame update
    void Update()
    {
        //Add the fps of this frame to the frameRates array
        frameRates[frameRateIterator] = (int)(1f / Time.deltaTime);
        frameRateIterator++;
        
        //Once we have sampled the desired number of fps values, reset the framerate iterator to record a fresh array of samples
        if (frameRateIterator >= numberOfFramesToAverage)
        {
            frameRateIterator = 0;
        }
        
        //We only update the values in the ui periodically, it would be difficult to read otherwise
        if (uiRefreshTimer > (1f / uiRefreshesPerSecond))
        {
            uiRefreshTimer = 0f;
            
            int frameRateTotal=0;
            for(int i = 0; i < numberOfFramesToAverage; i++)
            {
                frameRateTotal += frameRates[i];
            }
            int frameRate= frameRateTotal/numberOfFramesToAverage;
            frameRateCounter.text = frameRate + " FPS";

            if(totalReservedMemory.Valid) reserveMemory.text    = $"Total Reserved Memory: {totalReservedMemory.LastValue/(1024*1024)}MB";
            if(currentMemoryUsage.Valid) usedMemory.text        = $"Total Used Memory: {currentMemoryUsage.LastValue/(1024*1024)}MB";
        }
        uiRefreshTimer += Time.deltaTime;
    }
}
