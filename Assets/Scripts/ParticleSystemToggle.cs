using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//This periodically enables and disables the particle system attatched to this gameobject
public class ParticleSystemToggle : MonoBehaviour
{
    [SerializeField]
    private float period;
    private bool active;
    private float timer;
    private ParticleSystem particleSystem;
    
    void Start()
    {
        particleSystem = gameObject.GetComponent<ParticleSystem>();
    }

    void Update()
    {
        timer += Time.deltaTime;
        if (timer > period)
        {
            active = !active;
            if (active) particleSystem.Stop();
            else particleSystem.Play();
            timer = 0f;
        }
    }
}
