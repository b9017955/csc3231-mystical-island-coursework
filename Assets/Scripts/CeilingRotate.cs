using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Rotates the ceiling of the temple in a sinusoidal pattern
public class CeilingRotate : MonoBehaviour
{
    public bool clockwise;
    public float rotationRate;
    
    void Update()
    {
        float rotationDirection = clockwise ? 1f : -1f;
        float rotationAngle = rotationDirection * rotationRate * Mathf.Abs(Mathf.Sin(Time.timeSinceLevelLoad)) / 9f; //Using Abs() function reflects the sine curve in the x axis and results in a nice stop/start movement
        transform.Rotate(0, 0, rotationAngle);
    }
}
