using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UFOController : MonoBehaviour
{
    public float wobbleStrength;
    public float wobbleSpeed;
    public float moveSpeed;
    public float wanderDistance;
    public float maxDisplacement;//Max this unit can wander from its start position

    private Vector3 startPos;

    public float desiredProximity;
    private Vector3 objective = Vector3.zero;
    

    // Start is called before the first frame update
    void Start()
    {
        startPos = transform.position;
        GenerateNewWanderObjective();
    }

    // Update is called once per frame
    void Update()
    {
        //Add a wobble
        transform.rotation = Quaternion.Euler(
            wobbleStrength * Mathf.Sin(Time.timeSinceLevelLoad * wobbleSpeed), 
            0,
            wobbleStrength * Mathf.Cos(Time.timeSinceLevelLoad * wobbleSpeed)
        );
        MovementManager();
    }

    public void MovementManager()
    {      
        Vector3 objectiveRelativePosition = transform.InverseTransformPoint(objective);

        //If this gameobject has arrived at the objective, generate a new objective
        if (Mathf.Abs(Vector3.SqrMagnitude(objective-transform.position))< desiredProximity * desiredProximity)//SqrMagnitude() is used because it is more efficient
        {
            GenerateNewWanderObjective();
        }
        else
        {
            transform.Translate((objective-transform.position).normalized * moveSpeed * Time.deltaTime);//Move toward the objective
        }
    }

    void GenerateNewWanderObjective()
    {
        Vector3 randomVector = Random.insideUnitSphere;
        objective = new Vector3(
            randomVector.x * wanderDistance,
            0,
            randomVector.z * wanderDistance
            );
        objective += transform.position;

        //Constrain the coordinates of the new objective to within [maxDisplacement] of the gameObjects starting position
        objective.x = Mathf.Clamp(objective.x, startPos.x - maxDisplacement, startPos.x + maxDisplacement);
        objective.z = Mathf.Clamp(objective.z, startPos.z - maxDisplacement, startPos.z + maxDisplacement);
    }
}
