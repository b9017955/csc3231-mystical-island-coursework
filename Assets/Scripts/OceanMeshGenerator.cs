using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

//Used to generate the ocean mesh once, saves it as an asset
public class OceanMeshGenerator : MonoBehaviour
{
    public float yMultiplier = .1f;
    private int xLength = 100;
    private int zLength = 100;
    private MeshRenderer m;

    Vector3[] vertices;
    int[] triangles;

    Mesh mesh;

    // Start is called before the first frame update
    void Start()
    {
        mesh = new Mesh();
        GetComponent<MeshFilter>().mesh = mesh;
        m = GetComponent<MeshRenderer>();

        GenerateMesh();
        UpdateMesh();

        //Use only when saving this as a new asset
        /*AssetDatabase.CreateAsset(GetComponent<MeshFilter>().mesh, "Assets/OceanMesh.asset");
        AssetDatabase.SaveAssets();*/
    }

    void GenerateMesh()
    {
        vertices = new Vector3[(xLength + 1) * (zLength + 1)];

        for (int vertexIndex = 0, zPos = 0; zPos <= zLength; zPos++)
        {
            for (int xPos = 0; xPos <= xLength; xPos++)
            {
                vertices[vertexIndex] = new Vector3(xPos, 0, zPos);
                vertexIndex++;
            }
        }

        triangles = new int[xLength * zLength * 6];
        for (int vertexCounter = 0, triangleCounter = 0, zPos = 0; zPos < zLength; zPos++, vertexCounter++)
        {
            for (int xPos = 0; xPos < xLength; xPos++, vertexCounter++, triangleCounter += 6)
            {
                triangles[triangleCounter + 0] = vertexCounter + 0;
                triangles[triangleCounter + 1] = vertexCounter + xLength + 1;
                triangles[triangleCounter + 2] = vertexCounter + 1;
                triangles[triangleCounter + 3] = vertexCounter + 1;
                triangles[triangleCounter + 4] = vertexCounter + xLength + 1;
                triangles[triangleCounter + 5] = vertexCounter + xLength + 2;
            }
        }
    }

    void UpdateMesh()
    {
        mesh.Clear();
        mesh.vertices = vertices;
        mesh.triangles = triangles;

        mesh.RecalculateNormals();
    }

    // Update is called once per frame
    void Update()
    {
        //provide current time to shader
        m.material.SetFloat("movement", Time.realtimeSinceStartup);
    }
}
