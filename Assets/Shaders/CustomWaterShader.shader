Shader "Custom/CustomWaterShader"
{
    Properties
    {
        _MainTex ("Albedo (RGB)", 2D) = "white" {}
        _Glossiness ("Smoothness", Range(0,1)) = 0.5
        _Metallic ("Metallic", Range(0,1)) = 0.0

        _Color("Colour", Color)= (0,0,0,1) 
        _ShallowColour("Shallow Water Colour", Color) = (0.3, 0.8, 1, 0.7)//Colour at shallowest point
        _DeepColour("Deep Water Colour", Color) = (0.9, 0.4,1,0.7)//Colour at deepest point
        _FoamColour("Water Foam Colour", Color)= (1,1,1,1)// Colour of sea foam
        _MaxDepth("Max Depth", Float)=5//Water deeper than this is rendered as the _DeepColor
        _FoamDepth("Foam Depth", Float)=1//Water less deep than this is considered foam

        _Amplitude("Amplitude (Wave Height)", Range(0,2))=1.0
        _WaveSpeed("Speed", Range(-100,100))=100
    }
    SubShader
    {
        Tags { "RenderType"="transparent" }
        LOD 100

        //Blend One One
        //ZTest Always
        Pass
        {
            Cull Off//Water is transparent, it should not cull itself
            CGPROGRAM
            
            #pragma vertex      vert
            #pragma fragment    frag

            #include "UnityCG.cginc"
            float4 _ShallowColour;
            float4 _DeepColour;
            float4 _FoamColour;
            
            float _MaxDepth;//Anything deeper remains the _DeepColour
            float _FoamDepth;

            sampler2D _CameraDepthTexture;

            float4 _Color;
            float _Amplitude;
            float _WaveSpeed;


            struct appdata
            {
                float4 vertex   : POSITION;
                float2 uv       : TEXCOORD0;

                
            };

            struct v2f
            {
                float2 uv       : TEXCOORD0;
                float4 screenPos: TEXCOORD2;
                float4 vertex   : SV_POSITION;//System-value semantic indicates this is to hold the clip space position of the processed vertex

                
            };

            float movement;
            sampler2D   _MainTex;


            v2f vert(appdata v)
            {
                v2f output;
                //Add wave effect by manipulating vertices
                float4 localSpace = v.vertex;
                localSpace.y = sin(_WaveSpeed*_Time+localSpace.x)*_Amplitude;

                output.vertex    = UnityObjectToClipPos(localSpace);
                output.uv        = v.uv;
                output.screenPos = ComputeScreenPos(output.vertex);

                return output;
            }

            
            

            fixed4 frag(v2f input) : SV_Target
            {
                //Calculate water depth colour
                //fixed4 col = tex2D(_MainTex, input.uv);
                float existingDepth01 = tex2Dproj(_CameraDepthTexture, UNITY_PROJ_COORD(input.screenPos)).r;//Sample the camera depth texture
                float existingDepthLinear = LinearEyeDepth(existingDepth01);

                float depthDifference = existingDepthLinear - input.screenPos.w;
                float normalisedDepthDifference = saturate(depthDifference/_MaxDepth);

                float4 waterColour = lerp(_ShallowColour, _DeepColour, normalisedDepthDifference);
                
                if(depthDifference < _FoamDepth){
                    waterColour += _FoamColour;
                }
                
                return waterColour;
            }
            ENDCG
        }
    }
    SubShader
    {
        Tags { "RenderType"="transparent" }
        LOD 100

        CGPROGRAM
        // Physically based Standard lighting model, and enable shadows on all light types
        #pragma surface surf Standard fullforwardshadows

        // Use shader model 3.0 target, to get nicer looking lighting
        #pragma target 3.0

        sampler2D _MainTex;

        struct Input
        {
            float2 uv_MainTex;
        };

        half _Glossiness;
        half _Metallic;
        fixed4 _Color;

        // Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
        // See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
        // #pragma instancing_options assumeuniformscaling
        UNITY_INSTANCING_BUFFER_START(Props)
            // put more per-instance properties here
        UNITY_INSTANCING_BUFFER_END(Props)

        void surf (Input IN, inout SurfaceOutputStandard o)
        {
            // Albedo comes from a texture tinted by color
            fixed4 c = tex2D (_MainTex, IN.uv_MainTex) * _Color;
            o.Albedo = c.rgb;
            // Metallic and smoothness come from slider variables
            o.Metallic = _Metallic;
            o.Smoothness = _Glossiness;
            o.Alpha = c.a;
        }
        ENDCG
    }
}
