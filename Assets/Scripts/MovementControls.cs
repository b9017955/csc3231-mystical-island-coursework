using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class MovementControls : MonoBehaviour
{
    [Header("Unity Fields")]
    public CharacterController controller;
    public Transform feet;
    public LayerMask terrainLayer;
    public Camera camera;
    [Header("PLayer Fields")]
    [SerializeField]
    private float moveSpeed = .5f;
    private bool touchingGround;
    private float yVelocity;
    private Rigidbody r;
    bool flying;

    // Start is called before the first frame update
    void Start()
    {
        Debug.Log($"Layermask: {LayerMask.NameToLayer("Terrain")}");
        r = gameObject.GetComponent<Rigidbody>();
    }

    private void Update()
    {
        //toggle flying with "E"
        if (Input.GetKeyDown(KeyCode.E))
        {
            flying = !flying;
        }

        if (flying)
        {
            FlyingMovement();
        }
        else
        {
            WalkingMovement();
        }
    }

    void WalkingMovement()
    {
        float right = Input.GetAxis("Horizontal");
        float forward= Input.GetAxis("Vertical");

        Vector3 moveDir = transform.right * right + transform.forward * forward;
        controller.Move(moveDir.normalized * moveSpeed * Time.deltaTime);

        touchingGround = Physics.CheckSphere(feet.position, .5f, terrainLayer);

        if (!touchingGround)
        {
            yVelocity += Physics.gravity.y * Time.deltaTime;
            controller.Move(new Vector3(0, yVelocity * Time.deltaTime, 0));
        }
        else
        {
            yVelocity = 0f;
        }
    }

    void FlyingMovement()
    {
        float right = Input.GetAxis("Horizontal");
        float forward = Input.GetAxis("Vertical");

        Vector3 upDown = Vector3.zero;
        if (Input.GetKey(KeyCode.Space)) upDown = Vector3.up;
        if (Input.GetKey(KeyCode.LeftControl)) upDown = Vector3.down;
        float speedModifier = 1;
        if (Input.GetKey(KeyCode.LeftShift)) speedModifier = 2;//Left shift allows the player to move faster
        
        //Left-Right and Forwards-backwards movement is normalised, otherwise the player travels faster than they should if they strafe
        Vector3 moveDir = (transform.right * right + camera.transform.forward * forward).normalized + upDown;
        controller.Move(moveDir * moveSpeed * speedModifier * Time.deltaTime);
    }
}

