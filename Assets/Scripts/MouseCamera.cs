using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Controls mouse behaviour
public class MouseCamera : MonoBehaviour
{
    public float sensitivity = 100f;
    public Transform playerBody;
    private float yRotation;

    void Start()
    {
        //Hides the cursor
        Time.timeScale = 1f;
        Cursor.lockState = CursorLockMode.Locked;
    }

    void Update()
    {
        float mouseX = Input.GetAxis("Mouse X") * sensitivity * Time.deltaTime;
        float mouseY = Input.GetAxis("Mouse Y") * sensitivity * Time.deltaTime;

        yRotation -= mouseY;
        
        yRotation = Mathf.Clamp(yRotation, -90f, 90f);
        transform.localRotation = Quaternion.Euler(yRotation, 0f, 0f);
        playerBody.Rotate(Vector3.up, mouseX);
    }
}
